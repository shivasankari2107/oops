#include<iostream>
using namespace std;
class box{
         float length1,width1,height1;
         //cin>>length>>width>>height;

    public:
         void boxArea(float length,float width,float height){
            length1=length;width1=width;height1=height;
            cout<<"The area of the box is "<<endl;
            cout<<(2*((length*width)+(width*height)+(height*length)))<<endl;
        }
        void boxVolume(float length,float width,float height){
              cout<<"The volume of the box is "<<endl;
              cout<<length*width*height<<endl;
        }
        friend void displayBoxDimensions(box);
        void displayWelcomeMessage();

};
inline void box ::displayWelcomeMessage(){
    cout<<"Welcome"<<endl;
}
void displayBoxDimensions(box obj){
    cout<<"The dimensions of the box are :"<<endl;
    cout<<obj.length1<<" "<<obj.width1<<" "<<obj.height1<<endl;
}
int main(){
    box obj;
    float l,w,h;
    cout<<"Enter length,width,height : "<<endl;
    cin>>l>>w>>h;
    if(l>0 && w>0 && h>0){
        obj.boxArea(l,w,h);
        obj.boxVolume(l,w,h);
       
    }else{
        cout<<"invalid"<<endl;
    }
     displayBoxDimensions(obj);
    obj.displayWelcomeMessage();
    
}
