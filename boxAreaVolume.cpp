#include<iostream>
#ifndef boxArea
    #include "boxArea.h"
#endif
#ifndef boxVolume
    #include "boxVolume.h"
#endif
using namespace std;
int main(){
    float l,w,h;
    cout<<"Enter length,width,height values:"<<endl;
    cin>>l>>w>>h;
    cout<<"Area of the box :"<<endl;
    boxArea(l,w,h);
    cout<<"Volume of the box :"<<endl;
    boxVolume(l,w,h);
}