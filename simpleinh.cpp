#include<iostream>
using namespace std;
class King{
    public:
    King(){
        cout<<"hello kingdom"<<endl;
    }
    King(string name){
        cout<<name;
    }
    void position(){
        cout<<" is present king"<<endl;
    }

};
class Prince : public King{
    public:
    Prince(string name){
        cout<<name;
    }
    void pos(){
        cout<<" is future king"<<endl;
    }
};
int main()
{
    Prince obj("carlous");
    obj.pos();
    King obj2("Eric");
    obj.position();
}
