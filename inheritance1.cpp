#include<iostream>
using namespace std;
class teacher1{
    public:
    void displayteacher1(){
        cout<<"I am teacher1"<<endl;
    }
};
class teacher2{
    public:
    void displayteacher2(){
        cout<<"I am teacher2"<<endl;
    }
};
class student1:public teacher1{
    public:
    void displaystudent1(){
        cout<<" I am student1"<<endl;
    }
};
class student2:public student1{
    public:
    void displaystudent2(){
        cout<<"Iam student2"<<endl;
    }
};
class student3:public student1{
    public:
    void displaystudent3(){
        cout<<"Iam student3"<<endl;
    }
};
class student4:public teacher1,public teacher2{
    public:
    void displaystudent4(){
        cout<<"I am student4"<<endl;
    }
};
class student5:public student4{
    public:
    void displaystudent5(){
        cout<<"Iam student5"<<endl;
    }
};
int main(){
    student1 obj1;
    student2 obj2;
    student3 obj3;
    student4 obj4;
    student5 obj5;
    cout<<"Single Inheritance"<<endl;
    obj1.displayteacher1();
    obj1.displaystudent1();
    cout<<"Multi level Inheritance"<<endl;
    obj2.displayteacher1();
    obj2.displaystudent1();
    obj2.displaystudent2();
    cout<<"Multiple Inheritance"<<endl;
    obj4.displayteacher1();
    obj4.displayteacher2();
    obj4.displaystudent4();
    cout<<"Hierarchical Inheritance"<<endl;
    cout<<"left child"<<endl;
    obj2.displaystudent1();
    obj2.displaystudent2();
    cout<<"right child"<<endl;
    obj3.displaystudent1();
    obj3.displaystudent3();
    cout<<"Hybrid Inheritance"<<endl;
    obj5.displayteacher1();
    obj5.displayteacher2();
    obj5.displaystudent4();
    obj5.displaystudent5();
}

