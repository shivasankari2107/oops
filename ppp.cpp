#include <iostream>
using namespace std;

class Base {
  private:
    int priv = 1;

  protected:
    int prot = 2;

  public:
    int pub = 3;
 
    int getPriv() {      // function to access private member
      return priv;
    }
};

class PublicDerived : public Base {
  public:
    int getProt() {      // function to access protected member from Base
      return prot;
    }
};

class ProtectedDerived : protected Base {
  public:
    int getProt() {          // function to access protected member from Base
      return prot;
    }
    int getPub() {          // function to access public member from Base
      return pub;
    }
};

class PrivateDerived : private Base {
  public:
    int getProt() {            // function to access protected member from Base
      return prot;
    }
    int getPub() {          // function to access private member
      return pub;
    }
};
int main(){
    PublicDerived obj;
    std::cout << "private value = "<< obj.getPriv()<< std::endl;
    cout<<"protected value = "<<obj.getProt()<<endl;
    cout<<"public value = "<<obj.pub<<endl;
}

int main() {
  PublicDerived object1;
  cout << "Private = " << object1.getPVT() << endl;
  cout << "Protected = " << object1.getProt() << endl;
  cout << "Public = " << object1.pub << endl;
  return 0;
}