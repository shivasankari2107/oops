import java.lang.*;
import java.util.*;

class Evenodd {
    public static void main(String args[]) {
        Evenandodd();
    }

    static void Evenandodd() {
        System.out.println("Enter a value");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        if (a % 2 == 0) {
            System.out.println("Given number " + a + " is even");
        } else {
            System.out.println("Given number " + a + " is odd");
        }
    }
}
