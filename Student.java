import java.lang.*;
import java.util.*;
public class Student {
   String name;
   int rollno;
   double percentage;
   String collegename;
   int collegecode;
   Student(String n,int r,double p,String c,int code){
       name=n;
       rollno=r;
       percentage=p;
       collegename=c;
       collegecode=code;
       System.out.println(“\t student details are \t”);
       System.out.println("fullname : "+name);
       System.out.println("rollno : "+rollno);
       System.out.println("sempercentage : "+percentage);
       System.out.println("collegename : "+collegename);
       System.out.println("collegecode: "+collegecode);
   } 
   protected void finalize()  {
       System.out.println("destructor called");
   }

   public static void main(String args[]) {
       Scanner sc = new Scanner(System.in);
       System.out.println("enter the student name : ");
       String s = sc.next();
       System.out.println("enter collegename : ");
       String c = sc.next();
       System.out.println("enter rollno : ");
       int r = sc.nextInt();
       System.out.println("enter collegecode : ");
       int code = sc.nextInt();
       System.out.println("enter sempercentage : ");
       double p = sc.nextInt();
       Student me = new Student(s,r,p,c,code);
   }
}
