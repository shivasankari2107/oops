import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;


public class ImageReaderExample {
    public static void main(String[] args) throws IOException {
        // Specify the path to the input image file
        File inputFile = new File("input.jpg");


        // Get an ImageInputStream from the input file
        ImageInputStream imageInputStream = ImageIO.createImageInputStream(inputFile);


        // Get an Iterator of ImageReaders for the input image format
        Iterator<ImageReader> imageReaderIterator = ImageIO.getImageReaders(imageInputStream);


        // Iterate over the ImageReaders and print information about them
        while (imageReaderIterator.hasNext()) {
            ImageReader imageReader = imageReaderIterator.next();
            System.out.println("Image reader format name: " + imageReader.getFormatName());
            System.out.println("Image reader class name: " + imageReader.getClass().getName());
        }


        // Close the ImageInputStream
        imageInputStream.close();
    }
}
