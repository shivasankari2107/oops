import javax.imageio.ImageIO;  
import java.io.File;  
import java.io.IOException;  
import java.awt.image.BufferedImage;  
public class ImageIOExample   
{      
public static void main(String args[])  
{  
BufferedImage image = null;  
try   
{  
//we can either use URL or File for reading image using ImageIO  
File imagefile = new File("C:\\Users\\Anubhav\\Desktop\\image\\aa.jpg");  
image = ImageIO.read(imagefile);  
//ImageIO Image write Example in Java  
ImageIO.write(image, "jpg", new File("C:\\Users\\Anubhav\\Desktop\\image\\ab.gif"));  
ImageIO.write(image, "png", new File("C:\\Users\\Anubhav\\Desktop\\image\\ds.jpeg"));  
ImageIO.write(image, "gif", new File("C:\\Users\\Anubhav\\Desktop\\image\\hg.png"));  
ImageIO.write(image, "jpeg", new File("C:\\Users\\Anubhav\\Desktop\\image\\tt.png"));  
}   
catch (IOException e)   
{  
e.printStackTrace();  
}  
System.out.println("Success");  
}  
}  
