package pack;


import java.applet.Applet;
import java.awt.Graphics;


public class BasicApplet extends Applet {
    public void paint(Graphics g) {
        g.drawString("Welcome to applet", 150, 150);
    }
}
/*
<applet code="BasicApplet.class" width="300" height="300">
</applet>
*/
