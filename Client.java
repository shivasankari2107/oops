package pack;
public class Client {
    private String name;
    private String details;
    private String address;
    private String contactNumber;


    public Client() {}


    public Client(String name, String details, String address, String contactNumber) {
        this.name = name;
        this.details = details;
        this.address = address;
        this.contactNumber = contactNumber;
    }


    public void processRequest(int requestId, String name, String details, String address, String contactNumber) {
        // Process request here
        System.out.println("Client request processed.");
    }


    public void validateTransaction(String name) {
        // Validate transaction here
        System.out.println("Transaction validated.");
    }


    public void addSavings(String date) {
        // Add savings here
        System.out.println(" savings added on " + date + ".");
    }


    public void verifyAccount(String name2, String accType) {
        System.out.println("Account verified");
        System.out.println("details are:\n"+name2+"\n"+accType);
    }


    public void secureAccount(String name2, String accType) {
        System.out.println("Account secured");
        System.out.println("details are:\n"+name2+"\n"+accType);
    }


    public void accommodateRequest(String name2, String accType) {
        System.out.println("Request accepted");
    }
}


