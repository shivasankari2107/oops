#include<iostream>
using namespace std;
class AccessSpecifierDemo{
    private:
    int priVar;
    protected:
    int proVar;
    public:
    int pubVar;
    void setVar(int priValue,int proValue,int pubValue);
    void getVar();
};
void AccessSpecifierDemo::setVar(int priValue,int proValue,int pubValue){
    priVar=priValue;
    proVar=proValue;
    pubVar=pubValue;
}
void AccessSpecifierDemo::getVar(){
    cout<<"private value is : "<<priVar<<endl;
    cout<<"protected value is : "<<proVar<<endl;
    cout<<"public value is : "<<pubVar<<endl;
}
int main(){
    AccessSpecifierDemo spe;
    spe.setVar(2,3,4);
    spe.getVar();
}