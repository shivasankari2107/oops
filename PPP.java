class propup {
    public int x = 90;
    protected int y = 45;
    // private int z = 10;
}

class A extends propup {
    public void display() {
        System.out.println(x);
    }

    public void display1() {
        System.out.println(y);
    }

    public void display2() {
        // System.out.println(z);
    }
}

class B extends propup {
    protected void show() {
        System.out.println(x);
    }

    protected void show1() {
        System.out.println(y);
    }

    protected void show2() {
        // System.out.println(z);
    }
}

class C extends propup {
    private void show3() {
        System.out.println(x);
    }

    private void show4() {
        System.out.println(y);
    }

    // private void show5() {
    // System.out.println(z);

    public static void main(String[] args) {
        C obj = new C();
        obj.show3();
        obj.show4();
        // obj.show5();
    }
}
// }
}
// }