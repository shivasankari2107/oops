class AccessSpecifierDemo{
    public
    int pubVar;
   private
    int priVar;
   protected
    int proVar;
   public
   void setVar(int priValue,int proValue,int pubValue){
    priVar=priValue;
    proVar=proValue;
    pubVar=pubValue;
   }
   void getVar(){
    System.out.println("private value is "+priVar);
    System.out.println("protected value is "+proVar);
    System.out.println("public value is "+pubVar);
   }
   public static void main(String[] args) {
     AccessSpecifierDemo obj = new AccessSpecifierDemo();
    obj.setVar(10,20,30);
    obj.getVar();
   }
}
