import java.awt.*;
import java.awt.event.*;
import java.util.*;


import pack.Client;


import java.lang.*;


 
public class OnlineBanking extends Frame implements ActionListener {


    Label userLabel, passLabel, nameLabel, detailLabel, addressLabel, contactLabel;
    TextField userText, passText, nameText, detailText, addressText, contactText;
    Button loginBtn, addClientBtn, validateTransBtn, addSavingsBtn, verifyAccBtn, secureBtn, accommodateBtn;
   
    OnlineBanking() {
        // Create labels
        userLabel = new Label("Username:");
        passLabel = new Label("Password:");
        nameLabel = new Label("Name:");
        detailLabel = new Label("Details:");
        addressLabel = new Label("Address:");
        contactLabel = new Label("Contact Number:");
       
        // Create text fields
        userText = new TextField();
        passText = new TextField();
        nameText = new TextField();
        detailText = new TextField();
        addressText = new TextField();
        contactText = new TextField();
       
        // Set password field echo char
        passText.setEchoChar('*');
       
        // Create buttons
        loginBtn = new Button("Login");
        addClientBtn = new Button("Add Client");
        validateTransBtn = new Button("Validate Transaction");
        addSavingsBtn = new Button("Add Savings");
        verifyAccBtn = new Button("Verify Account");
        secureBtn = new Button("Secure");
        accommodateBtn = new Button("Accommodate");
       
        // Set button actions
        loginBtn.addActionListener(this);
        addClientBtn.addActionListener(this);
        validateTransBtn.addActionListener(this);
        addSavingsBtn.addActionListener(this);
        verifyAccBtn.addActionListener(this);
        secureBtn.addActionListener(this);
        accommodateBtn.addActionListener(this);
       
        // Set layout
        setLayout(new GridLayout(5, 9));
       
        // Add components
        add(userLabel);
        add(userText);
        add(passLabel);
        add(passText);
        add(loginBtn);
        add(new Label());
        add(nameLabel);
        add(nameText);
        add(detailLabel);
        add(detailText);
        add(addressLabel);
        add(addressText);
        add(contactLabel);
        add(contactText);
        add(addClientBtn);
        add(validateTransBtn);
        add(addSavingsBtn);
        add(verifyAccBtn);
        add(secureBtn);
        add(accommodateBtn);
       
        // Set window properties
        setTitle("Online Banking System");
        setSize(700, 700);
        setVisible(true);
    }
   
    public void actionPerformed(ActionEvent e) {
        // Get button source
        Button source = (Button)e.getSource();
       
        // Check button source and perform action
        if (source == loginBtn) {
            String username = userText.getText();
            String password = passText.getText();
            System.out.println("Username: " + username);
            System.out.println("Password: " + password);
        }
        else if (source == addClientBtn) {
            String name = nameText.getText();
            String details = detailText.getText();
            String address = addressText.getText();
            String contactNumber = contactText.getText();
            Client cr = new Client();
            cr.processRequest(10, name, details, address, contactNumber);
        }
        else if (source == validateTransBtn) {
            String name = nameText.getText();
            Client cr = new Client();
            cr.validateTransaction(name);
        }
        else if (source == addSavingsBtn) {
            String date = new Date().toString();
            Client cr = new Client();
            cr.addSavings(date);
        }
        else if (source == verifyAccBtn) {
            String name = nameText.getText();
            String accType = detailText.getText();
            Client cr = new Client();
            cr.verifyAccount(name, accType);
        }
        else if (source == secureBtn) {
            String name = nameText.getText();
            String accType = detailText.getText();
            Client cr = new Client();
            cr.secureAccount(name, accType);
        }
        else if (source == accommodateBtn) {
            String name = nameText.getText();
            String accType = detailText.getText();
            Client cr = new Client();
            cr.accommodateRequest(name, accType);
        }
    }
        public static void main(String[] args) {
        new OnlineBanking();
       
    }
}
