import javax.imageio.ImageIO;
import java.util.Arrays;


public class ImageReaderFormatNamesExample {
    public static void main(String[] args) {
        // Get the list of supported image format names
        String[] formatNames = ImageIO.getReaderFormatNames();
        System.out.println("Supported Image Format Names: " + Arrays.toString(formatNames));
    }
}
