public class Boxunb {
  public static void main(String[] args) {
    // Boxing: converting a primitive type to an object type
    int i = 10;
    Integer integer = Integer.valueOf(i);
    System.out.println("Boxed integer: " + integer);


    // Unboxing: converting an object type to a primitive type
    Double d = Double.valueOf(3.14);
    double x = d.doubleValue();
    System.out.println("Unboxed double: " + x);
  }
}
