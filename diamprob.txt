#include<iostream>
using namespace std;
class First
{
public:
    void display()
        {
            cout<<"Displaying Method in A ";
        }
};
class Second:virtual public First
{
    public:
    void show()
        {
            cout<<"Showing Method in B ";
        }
};
class Third:virtual public First
{
 
};
class Fourth:public Second,public Third
{
 
};
int main()
{
    Fourth obj;
    obj.display();
 
    return 0;
}